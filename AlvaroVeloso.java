class AlvaroVeloso{
    String nome;
    String sobrenome;
    int semestre;
    
    public AlvaroVeloso(String nome, String sobrenome, int semestre){
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.semestre = semestre;
    }
    
    String nomeCompleto(){
        return  this.nome + " " + this.sobrenome;
    }
    
    int getSemestre(){
        return this.semestre;
    }
}
